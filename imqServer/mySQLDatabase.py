import mysql.connector 
import json
from dataBaseService import *
from dbConfiguration import *
from exception import *
from topicHandler import *
from queueHandler import *

class mySQLDatabase(database):
    def connectToDataBase(self):
        dbHandler = mysql.connector.connect(
                     host = HOST,
                     user = USER,
                     passwd = PASSWORD,
                     database = DATABASE )
        return dbHandler
    
    def disconnectFromDataBase(self, dbHandler):
        dbHandler.close()

    def getCursorObject(self, dbHandler):
        cursorObject = dbHandler.cursor() 
        return cursorObject
    
    def createClientSpecificTable(self, cursorObject, clientId):
        tableName = 'Client' + '_' + clientId
        clientRecord = """CREATE TABLE """ + tableName + """( 
                   serial_no  INT PRIMARY KEY AUTO_INCREMENT, 
                   timestamp VARCHAR(50), 
                   data VARCHAR(200)
                   )"""
        cursorObject.execute(clientRecord)
    
    def saveDataIntoClientTable(self, cursorObject, clientId, data):
        tableName = 'Client' + '_' + clientId
        clientRecord =  ("INSERT INTO {} (serial_no, timestamp, data) VALUES(NULL, %s, %s)".format(tableName))
        cursorObject.execute(clientRecord, data)
    
    def saveDataIntoRootTable(self, cursorObject, clientId):
        tableName = 'Client' + '_' + clientId
        data = (clientId, tableName)
        clientRecord =  "INSERT INTO registeredClient (ID, clientTableName) VALUES(%s, %s)"
        cursorObject.execute(clientRecord, data)

    def isTableExist(self, clientId, cursorObject):
        tableName = 'Client' + '_' + clientId
        query = ("SHOW TABLES FROM client LIKE '{}'".format(tableName))
        cursorObject.execute(query)
        if len(cursorObject.fetchall()) > 0:
            return True
        else:
            return False  
    
    def createQueueSpecificTable(self, cursorObject, tableName):
        clientRecord = """CREATE TABLE """ + tableName + """( 
                   client  INT PRIMARY KEY, 
                   location INT
                   )"""
        cursorObject.execute(clientRecord)
    
    def saveTopicIntoList(self, cursorObject, topic):
        queue = json.dumps(topic.queue.__dict__)
        topic.queue = queue
        topic = json.dumps(topic.__dict__)
        data = (json.loads(topic)['name'], topic)
        clientRecord =  "INSERT INTO topic (name, topicObject) VALUES(%s, %s)"
        cursorObject.execute(clientRecord, data)
    
    def getTopicList(self, cursorObject):
        query = "SELECT name from topic;"
        cursorObject.execute(query)
        topicList = cursorObject.fetchall()
        return topicList
    
    def deleteEntry(self, cursorObject, topicName):
        query = ("DELETE FROM topic WHERE name='{}'".format(topicName))
        cursorObject.execute(query)

    def savePullLocationInQueue(self, cursorObject, data, tableName):
        record = (data[0], data[1])
        clientRecord =  ("INSERT INTO {} (client, location) VALUES(%s, %s)".format(tableName))
        cursorObject.execute(clientRecord, record)
    
    def registerUserWithTopic(self, cursorObject, topicName, clientId):
        query = ("SELECT topicObject from topic WHERE name = '{}'".format(topicName))
        cursorObject.execute(query)
        topicObject = cursorObject.fetchall()
        try:
            topicObject = json.loads(topicObject[0][0])
        except:
            raise imqServerException('Topic does not exist on imq Server')
        if clientId not in topicObject['registeredClient']:
            topicObject['registeredClient'].append(clientId)
        else:
            raise imqServerException('You are already registerd')
        topicName = topicObject['name']
        self.savePullLocationInQueue(cursorObject, [clientId, 0], topicName+'_queue')
        topicObject = json.dumps(topicObject)
        data = (topicName, topicObject)
        self.deleteEntry(cursorObject, topicName)
        query =  "INSERT INTO topic (name, topicObject) VALUES(%s, %s)"
        cursorObject.execute(query, data)
    
    def getRegisteredTopics(self, cursorObject, clientId):
        topicList = []
        query = "SELECT topicObject from topic;"
        cursorObject.execute(query)
        topicObjects = cursorObject.fetchall()
        for topic in topicObjects:
            if clientId in json.loads(topic[0])['registeredClient']:
                topicList.append(json.loads(topic[0])['name'])
        return topicList
    
    def getPullLocation(self, cursorObject, topicName, client):
        tableName = topicName + '_queue'
        query = ("SELECT location from {} where client = '{}'".format(tableName, client))
        cursorObject.execute(query)
        location = cursorObject.fetchall()[0][0]
        query = ("UPDATE {} SET location = '{}' WHERE client = '{}'".format(tableName, int(location + 1), client))
        cursorObject.execute(query)
        return location

    