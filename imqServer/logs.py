from datetime import datetime

class log:
    def getCurrentTime(self):
        now = datetime.now()
        currentTime = now.strftime("%H:%M:%S")
        return currentTime

    def saveMessageinFile(self, fileName, message):
        fileHandle = open(fileName, "a")
        currentTime = self.getCurrentTime()
        fileHandle.write(str(currentTime) + " " + message + "\n")
        fileHandle.close()
    
    def readEachLineinFile(self, fileName):
        fileHandle = open(fileName, 'r') 
        lines = fileHandle.readlines()
        return lines 
        
