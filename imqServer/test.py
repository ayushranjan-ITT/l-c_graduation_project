import unittest 
from library import imqServer
import socket

class ImqServerTest(unittest.TestCase):
    def test_createSocket(self):
        demoSocket = socket.socket()
        demoSocket.close()
        imqServerInstance = imqServer()
        socketInstance = imqServerInstance.createSocket()
        imqServerInstance.closeSocket(socketInstance)
        self.assertEqual(type(demoSocket), type(socketInstance))

    def test_bindSocket(self):
        imqServerInstance = imqServer()
        socketInstance = imqServerInstance.createSocket()
        imqServerInstance.bindSocket(socketInstance)
        self.assertEqual(('127.0.0.1', 1233), socketInstance.getsockname())
        imqServerInstance.closeSocket(socketInstance)
    
    def test_getImqPacket(self):
        imqServerInstance = imqServer()
        imqPacket = imqServerInstance.getImqPacket('Dummy Message', '127.0.0.1:1233', '127.0.0.1:5454')
        self.assertEqual('127.0.0.1:5454', imqPacket.getDestinationUri())


if __name__ == '__main__': 
	unittest.main() 