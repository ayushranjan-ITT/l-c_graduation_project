from library import *

def main():
    serverInstance = imqServer()
    serverSocket = serverInstance.createSocket()
    serverInstance.bindSocket(serverSocket)
    print('Listening for a Connection..')
    serverInstance.listening(serverSocket)
    while True:
        client, address = serverInstance.acceptConnection(serverSocket)
        print('Connected to: ' + address[0] + ':' + str(address[1]))
        filename = str(address[1]) + ".txt"
        imqPacket = serverInstance.getImqPacket('Welcome to the Server\n', 
                                                '127.0.0.1:1233', 
                                                str(address[0]) + ':' + str(address[1]))
        start_new_thread(serverInstance.serveClientRequest, (client, filename, imqPacket))
    serverInstance.closeSocket(serverSocket)

main()