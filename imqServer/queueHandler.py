import json
from datetime import datetime, date

EXPIRY_DAYS = 10

class queue:
    def __init__(self, name):
        self.name = name
        self.data = []
    
    @staticmethod
    def cleanQueue(cursorObject):
        query = ("SELECT topicObject FROM topic")
        cursorObject.execute(query)
        topicInstanceList = cursorObject.fetchall()
        for topicInstance in topicInstanceList:
            topicObject = json.loads(topicInstance[0])
            topicName = topicObject['name']
            queueInstance = json.loads(topicObject['queue'])
            index = 0
            while index < len(queueInstance['data']):
                message = json.loads(queueInstance['data'][index])
                manufacturedTime = datetime.strptime(message['manufacturedTime'].split(' ')[0], '%Y-%m-%d')
                now = datetime.now()
                days = (now - manufacturedTime).days
                if days >= EXPIRY_DAYS:
                    print('moved to dead queue')
                    del queueInstance['data'][index]
                    index = index - 1
                    queue.writeIntoDeadQueue(cursorObject, message['message'])
                else:
                    json.dumps(queueInstance['data'][index])
                index = index + 1
            queueInstance = json.dumps(queueInstance)
            del topicObject['queue']
            topicObject['queue'] = queueInstance
            topicObject = json.dumps(topicObject)
            query = ("UPDATE topic SET topicObject = '{}' WHERE name = '{}'".format(topicObject, topicName))
            cursorObject.execute(query)
        return 'successfull cleared Queue'
    
    @staticmethod
    def writeIntoDeadQueue(cursorObject, expiredMsg):
        data = (expiredMsg, )
        query = ("INSERT INTO deadqueue (message) VALUES(%s)")
        cursorObject.execute(query, data)

