from exception import *
from queueHandler import queue
import json

class topic:
    def __init__(self, name, queue):
        self.name = name
        self.registeredClient = []
        self.queue = queue
    
    @staticmethod
    def pushMessage(cursorObject, data, dbHandler):
        topicName = data.split('#')[1]
        query = ("SELECT topicObject FROM topic WHERE name='{}'".format(topicName))
        cursorObject.execute(query)
        topicInstance = cursorObject.fetchall()
        topicObject = json.loads(topicInstance[0][0])
        queueInstance = json.loads(topicObject['queue'])        
        dbHandler.deleteEntry(cursorObject, topicName)
        queueInstance['data'].append(data.split('#')[2])
        queueInstance = json.dumps(queueInstance)
        del topicObject['queue']
        topicObject['queue'] = queueInstance
        topicObject = json.dumps(topicObject)
        data = (topicName, topicObject)
        query =  "INSERT INTO topic (name, topicObject) VALUES(%s, %s)"
        cursorObject.execute(query, data)
        return 'Message pushed successfully'
    
    @staticmethod
    def pullMessage(cursorObject, topicName, index):
        query = ("SELECT topicObject from topic where name = '{}'".format(topicName))
        cursorObject.execute(query)
        topicObject = cursorObject.fetchall()
        topicObject = json.loads(topicObject[0][0])
        queueInstance = json.loads(topicObject['queue'])
        try:
            imqMessage = json.loads(queueInstance['data'][index])
            return imqMessage['message']
        except:
            raise imqServerException('Sorry !! No new message to pull')
    
    @staticmethod
    def verifyClient(cursorObject, topicName, clientId):
        query = ("SELECT topicObject from topic where name = '{}'".format(topicName))
        cursorObject.execute(query)
        topicObject = cursorObject.fetchall()
        try:
            topicObject = json.loads(topicObject[0][0])
        except:
            raise imqServerException('Sorry !! No topics are available on the server yet')
        if clientId in topicObject['registeredClient']:
            return True
        else:
            return False