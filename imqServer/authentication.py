from logs import *

class registerClient:
    def register(self, id, password):
        fileName = 'allClientDetails.txt'
        data = id + ' ' + password
        logger = log()
        logger.saveMessageinFile(fileName, data)
    
    def validateConfirmPassword(self, password, confirmPassword):
        if password == confirmPassword:
            return True
        else:
            return False

class authenticateClient:
    def login(self, id, password):
        logger = log()
        lines = logger.readEachLineinFile('allClientDetails.txt')
        for line in lines:
            if line.strip().split(' ')[1] == id and line.strip().split(' ')[2] == password:
                return True
        return False

