import socket, pickle
import os, sys
from _thread import *
from logs import *
from authentication import *
from mySQLDatabase import *
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from imqProtocol.imqProtocol import *
from exception import *
from queueHandler import *
from topicHandler import *

HOST = '127.0.0.1'
PORT = 1233

class imqServer:
    def createSocket(self):
        serverSocket = socket.socket()
        return serverSocket
    
    def closeSocket(self, serverSocket):
        serverSocket.close()
    
    def bindSocket(self, serverSocket):
        try:
            serverSocket.bind((HOST, PORT))
        except socket.error as error:
            imqServerException(str(error)).showErrorMessage()
    
    def acceptConnection(self, serverSocket):
        return serverSocket.accept()

    def listening(self, serverSocket):
        serverSocket.listen(5)
    
    def sendToClient(self, connection, imqPacket):
        connection.sendall(pickle.dumps(imqPacket))
    
    def receiveFromClient(self, connection):
        data = connection.recv(2048)
        imqPacket = pickle.loads(data)
        return imqPacket.getData()
    
    def getImqPacket(self, data, sourceUri, destinationUri):
        return imqProtocol(data, sourceUri, destinationUri)
    
    def imqClientLogin(self, data):
        currentUserId = None
        id = data.split('#')[1]
        password = data.split('#')[2]
        auth = authenticateClient()
        if auth.login(id, password):
            reply = 'Login Successfull'
            currentUserId = id
        else:
            reply = 'Server Says: ' + 'Login Failed'
        return reply, currentUserId
    
    def imqClientRegister(self, data):
        id = data.split('#')[1]
        password = data.split('#')[2]
        confirmPassword = data.split('#')[3]
        auth = registerClient()
        if auth.validateConfirmPassword(password, confirmPassword):
            auth.register(id, password)
            reply = 'Server Says: ' + 'Registartion Successfull'
        else:
            reply = 'Server Says: ' + 'Password and Confirm Password should match'
        return reply
    
    def createTopic(self, data):
        topicName = data.split('#')[1]
        queueName = topicName + '_queue'
        queueInstance = queue(queueName)
        topicInstance = topic(topicName, queueInstance)
        db = mySQLDatabase()
        dbHandler = db.connectToDataBase()
        cursorObject = db.getCursorObject(dbHandler)
        db.createQueueSpecificTable(cursorObject, queueName)
        db.saveTopicIntoList(cursorObject, topicInstance)
        dbHandler.commit()
        db.disconnectFromDataBase(dbHandler)
        print('topic created successfully', topicInstance.name)
        print('Disconnected client having ID admin')
        return
    
    def getTopics(self, data):
        db = mySQLDatabase()
        dbHandler = db.connectToDataBase()
        cursorObject = db.getCursorObject(dbHandler)
        topicList = db.getTopicList(cursorObject)
        return topicList
    
    def registerClientWithTopic(self, data, userId):
        db = mySQLDatabase()
        dbHandler = db.connectToDataBase()
        cursorObject = db.getCursorObject(dbHandler)
        topicName = data.split('#')[1]
        try:
            db.registerUserWithTopic(cursorObject, topicName, userId)
        except imqServerException as imqError:
            return imqError.errorMessage
        dbHandler.commit()
        db.disconnectFromDataBase(dbHandler)
        print('Client Registered with topic:- ', topicName)
        reply = 'Server Says: ' + 'Registered Successfully'
        return reply
    
    def getRegisteredTopics(self, userId):
        db = mySQLDatabase()
        dbHandler = db.connectToDataBase()
        cursorObject = db.getCursorObject(dbHandler)
        topicList = db.getRegisteredTopics(cursorObject, userId)
        return topicList
    
    def push(self, data):
        db = mySQLDatabase()
        dbHandler = db.connectToDataBase()
        cursorObject = db.getCursorObject(dbHandler)
        reply = topic.pushMessage(cursorObject, data, db)
        dbHandler.commit()
        db.disconnectFromDataBase(dbHandler)
        return reply
    
    def verifyClient(self, topicName, clientId):
        db = mySQLDatabase()
        dbHandler = db.connectToDataBase()
        cursorObject = db.getCursorObject(dbHandler)
        try:
            reply = topic.verifyClient(cursorObject, topicName, clientId)
        except imqServerException as imqError:
            return imqError.errorMessage
        return reply
    
    def pull(self, topicName, client):
        db = mySQLDatabase()
        dbHandler = db.connectToDataBase()
        cursorObject = db.getCursorObject(dbHandler)
        index = db.getPullLocation(cursorObject, topicName, client)
        try:
            reply = topic.pullMessage(cursorObject, topicName, index)
        except imqServerException as imqError:
            return imqError.errorMessage
        dbHandler.commit()
        db.disconnectFromDataBase(dbHandler)
        return reply
    
    def cleanQueue(self):
        db = mySQLDatabase()
        dbHandler = db.connectToDataBase()
        cursorObject = db.getCursorObject(dbHandler)
        reply = queue.cleanQueue(cursorObject)
        dbHandler.commit()
        db.disconnectFromDataBase(dbHandler)
        return

    def serveClientRequest(self, connection, filename, imqPacket):
        currentUserId = None
        self.sendToClient(connection, imqPacket)
        logger = log()
        while True:
            data = self.receiveFromClient(connection)
            logger.saveMessageinFile(filename, data)
            if data.split('#')[0] == 'REGISTER':
                reply = self.imqClientRegister(data)
            elif data.split('#')[0] == 'LOGIN':
                reply, currentUserId = self.imqClientLogin(data)
            elif data.split('#')[0] == 'LOGOUT':
                connection.close()
                print('Disconnected client having ID ', currentUserId)
                return
            elif data.split('#')[0] == 'TOPIC-CREATION':
                self.createTopic(data)
                connection.close()
                return
            elif data.split('#')[0] == 'GetTopics':
                reply = self.getTopics(data)
            elif data.split('#')[0] == 'RegisterTopic':
                reply = self.registerClientWithTopic(data, currentUserId)
            elif data.split('#')[0] == 'GetRegisteredTopics':
                reply = self.getRegisteredTopics(currentUserId)
            elif data.split('#')[0] == 'PUSH':
                reply = self.push(data)
            elif data.split('#')[0] == 'PULL':
                reply = self.pull(data.split('#')[1], currentUserId)
            elif data.split('#')[0] == 'VERIFY':
                reply = self.verifyClient(data.split('#')[1], currentUserId)
            elif data.split('#')[0] == 'CLEAN':
                self.cleanQueue()
                connection.close()
                return
            else:
                reply = 'Server Says: ' + data
                db = mySQLDatabase()
                dbHandler = db.connectToDataBase()
                cursorObject = db.getCursorObject(dbHandler)
                if not(db.isTableExist(currentUserId, cursorObject)):
                    db.createClientSpecificTable(cursorObject, currentUserId)
                    db.saveDataIntoRootTable(cursorObject, currentUserId)
                    dbHandler.commit()
                data = (logger.getCurrentTime(), data)
                db.saveDataIntoClientTable(cursorObject, currentUserId, data)
                dbHandler.commit()
                db.disconnectFromDataBase(dbHandler)
                if not data:
                    break
            imqPacket.data = reply
            self.sendToClient(connection, imqPacket)

