from abc import ABC, abstractmethod 
  
class database(ABC): 
  
    def connectToDataBase(self): 
        pass
    
    def disconnectFromDataBase(self, dbHandler):
        pass
    
    def getCursorObject(self, dbHandler):
        pass
    
    def createClientSpecificTable(self, cursorObject, clientId):
        pass
    
    def saveDataIntoClientTable(self, cursorObject, clientId, data):
        pass
    
    def saveDataIntoRootTable(self, cursorObject, clientId):
        pass
    
    def isTableExist(self, clientId, cursorObject):
        pass
    
    def createQueueSpecificTable(self, cursorObject, tableName):
        pass
    
    def saveTopicIntoList(self, cursorObject, topic):
        pass