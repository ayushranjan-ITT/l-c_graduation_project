class imqProtocol:
    def __init__(self, data, sourceUri, destinationUri):
        self.data = data
        self.sourceUri = sourceUri
        self.destinationUri = destinationUri
    
    def getData(self):
        return self.data
    
    def getSourceUri(self):
        return self.sourceUri
    
    def getDestinationUri(self):
        return self.destinationUri