class imqClientException(Exception):
    def __init__(self, errorMessage):
        self.errorMessage = errorMessage
    
    def showErrorMessage(self):
        print(self.errorMessage)