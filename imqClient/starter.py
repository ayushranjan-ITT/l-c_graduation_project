from library import *
import os, sys, json
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from imqMessageFactory.imqMessageFactory import *
clearConsole = lambda: os.system('cls')


def displayClientMenu():
    print('Press 1 to list available topics on the imq Server')
    print('Press 2 to get the list of registered topics')
    print('Press 3 to get registered with a new Topic')
    print('Press 4 to connect with a topic')
    print('Press 5 to logout')

def displayMainMenu():
    print("Press REGISTER to register")
    print("Press LOGIN to login")

def displayAdminMenu():
    print('<-- Welcome Admin -->')
    print('Press 1 to create a new topic on IMQ Server')
    print('Press 2 for clean up process on the IMQ Server')
    message = input()
    if message == '1':
        topicName = input('Enter name of the topic to be created on the imqServer\n')
        message = 'TOPIC-CREATION' + '#' + topicName
    elif message == '2':
        message = 'CLEAN'
    else:
        message = 'Invalid'
    return message

def sendAndReceive(clientInstance, clientSocket, message):
    imqPacket = clientInstance.getImqPacket(message, imqClient.sourceUri, imqClient.destinationUri)
    clientInstance.sendToServer(clientSocket, imqPacket)
    imqPacket = clientInstance.receiveFromServer(clientSocket)
    message = imqPacket.getData()
    return message

def verifyTopic(topicName, clientInstance, clientSocket):
    message = 'VERIFY' + '#' + topicName
    message = sendAndReceive(clientInstance, clientSocket, message)
    return message

def displayTopicMenu(topicName, clientInstance, clientSocket):
    clearConsole()
    print('Type PUSH for pushing the message')
    print('Type PULL for pulling the message')
    print('Type BACK for Main Menu')
    while True:
        message = input('>> ')
        if message == 'PUSH':
            imqMsg = input('Enter the message you want to push\n')
            imqMessagePacket = imqMessage(imqMsg)
            imqMessagePacket = json.dumps(imqMessagePacket.__dict__)
            message = message + '#' + topicName + '#' + imqMessagePacket
            message = sendAndReceive(clientInstance, clientSocket, message)
            print(message)
        elif message == 'PULL':
            message = message + '#' + topicName
            message = sendAndReceive(clientInstance, clientSocket, message)
            print(message)
        elif message == 'BACK':
            return
        else:
            print('Please enter a valid option')


def main():
    clientInstance = imqClient()
    clientSocket = clientInstance.createSocket()
    print('Waiting for connection')
    clientInstance.connectToServer(clientSocket)
    imqPacket = clientInstance.receiveFromServer(clientSocket)
    imqClient.sourceUri = imqPacket.getDestinationUri()
    imqClient.destinationUri = imqPacket.getSourceUri()
    print(imqPacket.getData())
    displayMainMenu()
    while True:
        message = input('Say Something: ')
        if message == 'REGISTER':
            id = input('Enter your id :-')
            password = input('Enter your password :-')
            confirmPassword = input('Enter your password again :-')
            message = message + '#' + id + '#' + password + '#' + confirmPassword
        elif message == 'LOGIN':
            id = input('Enter your id :-')
            password = input('Enter your password :-')
            message = message + '#' + id + '#' + password
        else:
            if not(imqClient.isClientAuthenticated):
                print('Please first Login/Register')
                continue
        if message != '':
            message = sendAndReceive(clientInstance, clientSocket, message)
            print(message)
        if message == 'Login Successfull':
            imqClient.isClientAuthenticated = True
            clearConsole()
            if id == 'admin':
                message = displayAdminMenu()
                if message != 'Invalid':
                    imqPacket = clientInstance.getImqPacket(message, imqClient.sourceUri, imqClient.destinationUri)
                    clientInstance.sendToServer(clientSocket, imqPacket)
                    clientInstance.closeSocket(clientSocket)
                    print('Successfull operation')
                exit()
            displayClientMenu()
            while True:
                message = input('>> ')
                if message == '1':
                    message = 'GetTopics'
                    message = sendAndReceive(clientInstance, clientSocket, message)
                    print('--------------------------')
                    for topic in message:
                        print(topic[0])
                    print('--------------------------')
                elif message == '2':
                    message = 'GetRegisteredTopics'
                    message = sendAndReceive(clientInstance, clientSocket, message)
                    print('Registered topics are:- ')
                    for topic in message:
                        print(topic)
                elif message == '3':
                    topicName = input('Enter topic Name to register with :-  ')
                    message = 'RegisterTopic' + '#' + topicName
                    message = sendAndReceive(clientInstance, clientSocket, message)
                    print(message)
                elif message == '4':
                    topicName = input('Enter the topic name\n')
                    if verifyTopic(topicName, clientInstance, clientSocket) == True:
                        displayTopicMenu(topicName, clientInstance, clientSocket)
                        clearConsole()
                        displayClientMenu()
                    else:
                        print('Your are not registered with ', topicName)
                elif message == '5':
                    message = 'LOGOUT'
                    imqPacket = clientInstance.getImqPacket(message, imqClient.sourceUri, imqClient.destinationUri)
                    clientInstance.sendToServer(clientSocket, imqPacket)
                    clientInstance.closeSocket(clientSocket)
                    exit()
                else:
                    print('Please enter a valid choice')
    clientInstance.closeSocket(clientSocket)

main()