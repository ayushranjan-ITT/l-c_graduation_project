import unittest 
from library import imqClient
import socket

class ImqServerTest(unittest.TestCase):
    def test_createSocket(self):
        demoSocket = socket.socket()
        demoSocket.close()
        imqClientInstance = imqClient()
        socketInstance = imqClientInstance.createSocket()
        imqClientInstance.closeSocket(socketInstance)
        self.assertEqual(type(demoSocket), type(socketInstance))
    
    def test_getImqPacket(self):
        imqClientInstance = imqClient()
        imqPacket = imqClientInstance.getImqPacket('Dummy Message', '127.0.0.1:5454', '127.0.0.1:1233')
        self.assertEqual('127.0.0.1:1233', imqPacket.getDestinationUri())


if __name__ == '__main__': 
	unittest.main() 