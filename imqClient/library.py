import socket, pickle
import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from imqProtocol.imqProtocol import *
from exception import *

HOST = '127.0.0.1'
PORT = 1233

class imqClient:
    isClientAuthenticated = False
    sourceUri = None
    destinationUri = None

    def createSocket(self):
        clientSocket = socket.socket()
        return clientSocket
    
    def connectToServer(self, clientSocket):
        try:
            clientSocket.connect((HOST, PORT))
        except socket.error as error:
            imqClientException(str(error)).showErrorMessage()
    
    def receiveFromServer(self, clientSocket):
        try:
            Response = clientSocket.recv(1024)
            imqPacket = pickle.loads(Response)
            return imqPacket
        except socket.error as error:
            imqClientException(str(error)).showErrorMessage()
            self.closeSocket(clientSocket)
            exit()
    
    def sendToServer(self, clientSocket, imqPacket):
        clientSocket.send(pickle.dumps(imqPacket))
    
    def closeSocket(self, clientSocket):
        clientSocket.close()
    
    def getImqPacket(self, data, sourceUri, destinationUri):
        return imqProtocol(data, sourceUri, destinationUri)